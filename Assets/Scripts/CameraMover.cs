using UnityEngine;

public class CameraMover : MonoBehaviour
{
    [SerializeField] Vector3 secondPos;
    private Vector3 startPos;
    private bool isInSecondPos = false;

    private void Start()
    {
        startPos = transform.position;
    }

    public void ChangeCamera()
    {
        if (isInSecondPos) 
        {
            transform.position = startPos;
            isInSecondPos = false;
        } 
        else
        {
            transform.position = secondPos;
            isInSecondPos = true;
        }
    }
}
