using System;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    [SerializeField] Transform connectedPoint;
    private bool wasUsed = false;

    private void OnTriggerEnter(Collider other)
    {
        if (wasUsed) return;
        if (other.gameObject.GetComponent<Player>())
        {
            Transform player = other.GetComponent<Transform>();
            Vector3 tpPos = new Vector3(connectedPoint.position.x, player.position.y, connectedPoint.position.z);
            wasUsed = true;
            connectedPoint.GetComponent<Teleporter>().wasUsed = true;
            player.position = tpPos;

            Camera.main.GetComponent<CameraMover>().ChangeCamera();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!wasUsed) return;
        if (other.GetComponent<Player>())
            wasUsed = false;
    }
}